import jwt
import pendulum
import requests
from pprint import pprint

HOST = '192.168.2.5'
PROTOCOL = 'http'
USER = 'user'
PASSWORD = 'user'

METER_ID = 81
YEAR = 2018

if __name__ == '__main__':
    # Obtain a JSON web token
    response = requests.post(f'{PROTOCOL}://{HOST}/api/token-auth/', {'username': USER, 'password': PASSWORD}).json()
    token = response['token']
    # Perform an API request with the token as header (Authorization: JWT <token>)
    response = requests.get(
        f'{PROTOCOL}://{HOST}/api/meterlog/increments/?meterId={METER_ID}&year={YEAR}',
        headers={'Authorization': f'JWT {token}'}
    ).json()
    pprint(response)
    expires = jwt.JWT().decode(token, None, False)['exp']
    print(f'Token is valid until {pendulum.from_timestamp(expires)}')
